#include "main.h"

void operatorControl()
{
  bool autonPrevPressed = false;
  while (true)
  {
    if (joystickGetDigital(1, 7, JOY_UP) && !autonPrevPressed)
    {
      if (autonTask == NULL)
        autonTask = taskCreate(autonomous, TASK_DEFAULT_STACK_SIZE, NULL, TASK_PRIORITY_HIGHEST);
      else
        taskDelete(autonTask);
    }
    autonPrevPressed = joystickGetDigital(1, 7, JOY_UP);
    if (autonTask != NULL)
      printf("autonTask: %d\n", taskGetState(autonTask));
    if (autonTask == NULL || taskGetState(autonTask) == TASK_DEAD)
    {
      doDrivebase();
      doClaw();
      doFourbar();
      doFence();
      doLightpole();
    }
    delay(50);
  }
}