/** @file auto.c
 * @brief File for autonomous code
 *
 * This file should contain the user autonomous() function and any functions related to it.
 *
 * PROS contains FreeRTOS (http://www.freertos.org) whose source code may be
 * obtained from http://sourceforge.net/projects/freertos/files/ or on request.
 */

#include "main.h"

void autonomous()
{
    // Drive Forward for half a second
    motorSet(LEFT_DRIVE_MOTOR, 127);
    motorSet(RIGHT_DRIVE_MOTOR, -127);
    motorSet(FOURBAR_MOTOR_PORT, 127);
    delay(1200);
    motorSet(LEFT_DRIVE_MOTOR, 0);
    motorSet(RIGHT_DRIVE_MOTOR, 0);

    // Move the fourbar down to grab pipe bundle
    delay(3000);
    motorSet(FOURBAR_MOTOR_PORT, 0);

    // Drive backwards
    motorSet(LEFT_DRIVE_MOTOR, -127);
    motorSet(RIGHT_DRIVE_MOTOR, 127);
    delay(2000);
}
