#include "main.h"

void doLightpole()
{
    if (joystickGetDigital(1, 8, JOY_UP))
        motorSet(LIGHTPOLE_MOTOR, 60);
    else if (joystickGetDigital(1, 8, JOY_DOWN))
        motorSet(LIGHTPOLE_MOTOR, -60);
    else
        motorSet(LIGHTPOLE_MOTOR, 0);
}