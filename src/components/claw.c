#include "main.h"

void doClaw()
{
    static bool isClawOpen = false;
    static bool wasClawPressed = false;
    if (joystickGetDigital(1, 5, JOY_UP) && !wasClawPressed)
        isClawOpen = !isClawOpen;
    wasClawPressed = joystickGetDigital(1, 5, JOY_UP);

    if (isClawOpen)
        motorSet(CLAW_MOTOR_PORT, -40);
    else
        motorSet(CLAW_MOTOR_PORT, 127);
}