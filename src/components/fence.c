#include "main.h"

enum positionState
{
    Up,
    Mid,
    Down,
    Stow
};

const char fenceOffset = 57;

void doFence()
{
    static bool wasFencePressed = false;
    static enum positionState currentPosition = Stow;
    if (joystickGetDigital(1, 5, JOY_DOWN) && !wasFencePressed)
    {
        currentPosition++;
        currentPosition %= 3;
    }
    wasFencePressed = joystickGetDigital(1, 5, JOY_DOWN);

    switch (currentPosition)
    {
    case Up:
        motorSet(FENCE_LEFT_SERVO, 70 + fenceOffset);
        motorSet(FENCE_RIGHT_SERVO, -70 - fenceOffset);
        break;

    case Mid:
        motorSet(FENCE_LEFT_SERVO, -25 + fenceOffset);
        motorSet(FENCE_RIGHT_SERVO, 25 - fenceOffset);
        break;

    case Down:
        motorSet(FENCE_LEFT_SERVO, -127 + fenceOffset);
        motorSet(FENCE_RIGHT_SERVO, 127 - fenceOffset);
        break;
    case Stow:
        motorSet(FENCE_LEFT_SERVO, 127);
        motorSet(FENCE_RIGHT_SERVO, -127);
        break;
    default:
        motorSet(FENCE_LEFT_SERVO, 0);
        motorSet(FENCE_RIGHT_SERVO, 0);
    }
}